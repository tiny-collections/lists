package com.gitlab.collections.tiny.list.array;

import com.gitlab.collections.tiny.list.iterator.IntListIterator;

class IntArrayListIterator implements IntListIterator {
    private final IntArrayList list;
    private boolean modified;
    private int cursor;

    IntArrayListIterator(IntArrayList list, int startIndex) {
        this.list = list;
        this.cursor = startIndex;
        modified = false;
    }

    IntArrayListIterator(IntArrayList list) {
        this(list, 0);
    }

    @Override
    public void add(int e) {
        list.add(cursor, e);
    }

    @Override
    public boolean hasPrevious() {
        return cursor > 0;
    }

    @Override
    public int nextIndex() {
        return cursor;
    }

    @Override
    public int previous() {
        int e = list.get(cursor);
        modified = true;
        cursor--;
        return e;
    }

    @Override
    public int previousIndex() {
        return cursor;
    }

    @Override
    public void set(int e) {
        if (!modified) {
            throw new IllegalStateException();
        }

        list.set(cursor, e);
    }

    @Override
    public int nextInt() {
        int e = list.get(cursor);
        modified = true;
        cursor++;
        return e;
    }

    @Override
    public boolean hasNext() {
        return cursor < list.size();
    }
}
