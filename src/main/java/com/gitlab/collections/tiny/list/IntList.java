package com.gitlab.collections.tiny.list;

import com.gitlab.collections.tiny.core.IntCollection;
import com.gitlab.collections.tiny.list.iterator.IntListIterator;

/**
 * An ordered collection. The precise location of each element can be specified.
 */
public interface IntList extends IntCollection {
    /**
     * Gets a specific element from the list.
     *
     * @param index The index of the element to get.
     * @return The element at the index.
     * @throws IndexOutOfBoundsException Index is out of range (index < 0 || index >= size())
     */
    int get(int index);

    /**
     * Gets the index of the first element that equals the specified value.
     *
     * @param value The value to search for
     * @return The index of the element in the list or -1 if it was not found.
     */
    int indexOf(int value);

    /**
     * Gets the index of the last element that equals the specified value.
     *
     * @param value The value to search for
     * @return The index of the element in the list or -1 if it was not found.
     */
    int lastIndexOf(int value);

    /**
     * Returns a view of this list which encloses the specified range. The view will be backed by this list.
     *
     * @param fromIndex The inclusive start index.
     * @param toIndex The exclusive end index.
     * @return A view of the range of this list.
     */
    IntList subList(int fromIndex, int toIndex);

    /**
     * Returns a list iterator of the elements in this list. Starting with the first element.
     *
     * @return An index 0 list iterator.
     */
    default IntListIterator listIterator() {
        return listIterator(0);
    }

    /**
     * Returns a list iterator of the elements in this list. Starting with the specified index.
     *
     * @param index The index to start at.
     * @return A list iterator.
     */
    IntListIterator listIterator(int index);
}
