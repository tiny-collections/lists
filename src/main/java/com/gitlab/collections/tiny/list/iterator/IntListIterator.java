package com.gitlab.collections.tiny.list.iterator;

import java.util.PrimitiveIterator;

/**
 * Specialized type of {@link java.util.ListIterator}
 */
public interface IntListIterator extends PrimitiveIterator.OfInt {
    /**
     * Inserts an element before the element that would be returned by {@link #nextInt()}
     *
     * @param e The element to add
     */
    void add(int e);

    /**
     * Returns true if there are more elements when traversing in the reverse direction.
     *
     * @return True if the next call to {@link #previous()} will not throw.
     */
    boolean hasPrevious();

    /**
     * Returns the index of the element that would be returned by a call to {@link #nextInt()}
     *
     * @return The index of the next element.
     */
    int nextIndex();

    /**
     * Returns the previous item in the list and moves the cursor backwards.
     *
     * @return The previous element in the list
     */
    int previous();

    /**
     * Returns the index of the element that would be returned by a call to {@link #previous()}
     *
     * @return The index of the previous element.
     */
    int previousIndex();

    /**
     * Replaces the last element returned by either {@link #nextInt()} or {@link #previous()}
     *
     * @param e The new element to put into the list
     */
    void set(int e);
}
