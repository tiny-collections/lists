package com.gitlab.collections.tiny.list.array;

import com.gitlab.collections.tiny.core.IntCollection;
import com.gitlab.collections.tiny.core.compare.IntComparator;
import com.gitlab.collections.tiny.list.ModifiableIntList;
import com.gitlab.collections.tiny.list.iterator.IntListIterator;

import java.util.Arrays;
import java.util.PrimitiveIterator;
import java.util.Spliterator;

public class IntArrayList implements ModifiableIntList {
    private static final int INITIAL_SIZE = 10;
    private static final float SCALE_FACTOR = 1.5F;
    private static final int[] ZERO_ARRAY = new int[0];

    private int[] arr;
    private int realSize;

    public IntArrayList() {
        arr = ZERO_ARRAY;
    }

    public IntArrayList(int initialSize) {
        arr = new int[initialSize];
    }

    public IntArrayList(IntCollection c) {
        arr = c.toArray();
    }

    public IntArrayList(int[] c) {
        arr = Arrays.copyOf(c, c.length);
    }

    @Override
    public int get(int index) {
        rangeCheck(index);
        return arr[index];
    }

    @Override
    public int indexOf(int value) {
        for (int i = 0; i < realSize; i++) {
            if (arr[i] == value) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public int lastIndexOf(int value) {
        for (int i = realSize - 1; i >= 0; i--) {
            if (arr[i] == value) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public void add(int index, int element) {
        rangeCheck(index);
        ensureCapacity(realSize + 1);

        shiftRight(index, 1);
        arr[index] = element;
    }

    @Override
    public int removeIndex(int index) {
        rangeCheck(index);

        int old = arr[index];
        shiftLeft(index + 1, 1);
        return old;
    }

    @Override
    public int set(int index, int element) {
        rangeCheck(index);

        int old = arr[index];
        arr[index] = element;
        return old;
    }

    @Override
    public ModifiableIntList subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean addAll(int index, IntCollection c) {
        rangeCheck(index);
        ensureCapacity(realSize + c.size());

        if (c instanceof IntArrayList) {
            final IntArrayList l = (IntArrayList) c;
            System.arraycopy(l.arr, 0, arr, realSize, c.size());
        } else {
            System.arraycopy(arr, index, arr, realSize + c.size(), c.size());

            final PrimitiveIterator.OfInt itr = c.iterator();
            for (int i = 0; i < c.size(); i++) {
                arr[index + i] = itr.nextInt();
            }
        }

        return true;
    }

    @Override
    public void sort(IntComparator comparator) {

    }

    @Override
    public IntListIterator listIterator(int index) {
        return new IntArrayListIterator(this, index);
    }

    @Override
    public boolean contains(int i) {
        return indexOf(i) != -1;
    }

    @Override
    public PrimitiveIterator.OfInt iterator() {
        return new IntArrayListIterator(this);
    }

    @Override
    public int[] toArray() {
        return Arrays.copyOfRange(arr, 0, realSize);
    }

    @Override
    public int[] toArray(int[] ints) {
        if (ints.length >= realSize) {
            System.arraycopy(arr, 0, ints, 0, realSize);
            return ints;
        } else {
            return toArray();
        }
    }

    @Override
    public Spliterator.OfInt spliterator() {
        return null;
    }

    @Override
    public int size() {
        return realSize;
    }

    @Override
    public boolean add(int i) {
        ensureCapacity(realSize + 1);

        arr[realSize] = i;
        realSize++;
        return true;
    }

    @Override
    public boolean remove(int i) {
        final int idx = indexOf(i);

        if (idx == -1) {
            return false;
        } else {
            removeIndex(idx);
            return true;
        }
    }

    @Override
    public void clear() {
        arr = ZERO_ARRAY;
        realSize = 0;
    }

    private void rangeCheck(int index) {
        if (index < 0 || index >= realSize) {
            throw new IndexOutOfBoundsException("Index out of range: " + index);
        }
    }

    private void ensureCapacity(int size) {
        if (arr == ZERO_ARRAY) {
            if (size <= INITIAL_SIZE) {
                arr = new int[INITIAL_SIZE];
            } else {
                arr = new int[size];
            }

            return;
        }

        if (size <= arr.length) {
            return;
        }

        // Calculate new size
        int newSize = arr.length;
        while (newSize <= size) {
            newSize = (int) (SCALE_FACTOR * newSize);
        }

        // Create new array
        arr = Arrays.copyOf(arr, newSize);
    }

    private void shiftLeft(int firstIndex, int numOfPositions) {
        System.arraycopy(arr, firstIndex, arr, firstIndex - numOfPositions, realSize - firstIndex);
    }

    private void shiftRight(int firstIndex, int numOfPositions) {
        System.arraycopy(arr, firstIndex, arr, firstIndex + numOfPositions, realSize - firstIndex);
    }
}
