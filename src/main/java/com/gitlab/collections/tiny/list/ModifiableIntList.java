package com.gitlab.collections.tiny.list;

import com.gitlab.collections.tiny.core.IntCollection;
import com.gitlab.collections.tiny.core.ModifiableIntCollection;
import com.gitlab.collections.tiny.core.compare.IntComparator;

import java.util.Objects;
import java.util.function.IntUnaryOperator;

/**
 * A modifiable extension of an {@link IntList}
 */
public interface ModifiableIntList extends IntList, ModifiableIntCollection {
    /**
     * Inserts an element at the specified index. The current element (and any subsequent elements) will be shifted to
     * the right.
     *
     * @param index   The index to insert at.
     * @param element The element to insert.
     * @throws IndexOutOfBoundsException Index is out of range (index < 0 || index >= size())
     */
    void add(int index, int element);

    /**
     * Removes the element at the specified index.
     *
     * @param index The index of the element to remove.
     * @return The element that was removed.
     * @throws IndexOutOfBoundsException Index is out of range (index < 0 || index >= size())
     */
    int removeIndex(int index);

    /**
     * Replaces the element at the specified index with a new value.
     *
     * @param index   The index of the element to replace.
     * @param element The element to be stored.
     * @return The replaced element.
     * @throws IndexOutOfBoundsException Index is out of range (index < 0 || index >= size())
     */
    int set(int index, int element);

    @Override
    ModifiableIntList subList(int fromIndex, int toIndex);

    // Bulk Operations

    /**
     * Adds all the elements in the specified collection to this collection. Elements are inserted starting at the
     * specified index. Any element at or after the specified index will be shifted right to accommodate the elements of
     * the specified collection.
     *
     * @param index The index to insert at.
     * @param c     The collection of elements to insert.
     * @return True if this collection changed.
     * @throws IndexOutOfBoundsException Index is out of range (index < 0 || index >= size())
     */
    boolean addAll(int index, IntCollection c);

    /**
     * Replace each element of this list by applying an operator.
     *
     * @param operator The operator to apply.
     */
    default void replaceAll(IntUnaryOperator operator) {
        Objects.requireNonNull(operator);

        for (int i = 0; i < size(); i++) {
            set(i, operator.applyAsInt(get(i)));
        }
    }

    /**
     * Sorts the elements of this list according to the order produced by the specified comparator.
     *
     * @param comparator The comparator defining element order.
     */
    void sort(IntComparator comparator);
}
